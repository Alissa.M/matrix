#include <iostream>
#include <stdlib.h>
#include <cstring>
#include <string>
#include <fstream>
using namespace std;

int** polynomial(int **matrix0, size_t n, size_t m, string &str) {
	int index = 0, index1 = 0, noexp = 0;

	bool TF = false;

	int **matrix9 = new int*[n];
	for (int i = 0; i < n; i++)
		matrix9[i] = new int[m];

	int **matrix_ = new int*[n];
	for (int i = 0; i < n; i++)
		matrix_[i] = new int[m];

	if (str[0] == '-') {
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++) {
				matrix_[i][j] = 0;
			}
	}
	else {
		for (int i = 0; i < str.length(); i++) {
			switch (str[i]) {
			case 'x': {
				if (str[i + 1] == '^') {
					break;
				}
				else {
					for (int i = 0; i < n; i++)
						for (int j = 0; j < m; j++)
							matrix_[i][j] = (index + 1) * matrix0[i][j];
					index = 0;
				}
			} break;

			case '^': {
				noexp = 1;
				string strp = str;
				int x = 0;

				if ((isdigit(str[i + 2])) || (str[i + 2] != '\0')) {
					strp.erase(0, i + 1);
					if (str.length() > 1)
						strp.erase(2, str.length() - 2);
					x = atoi(strp.c_str()); //strp.c_str возвращает указатель на массив символов
				}
				else {
					strp.erase(0, i + 1);
					if (str.length() > 1)
						strp.erase(1, str.length() - 1);
					x = atoi(strp.c_str());
				}

				if ((x == 0) || (x == 1)) {
					if (x == 0) {
						for (int i = 0; i < n; i++) {
							for (int j = 0; j < m; j++) {
								matrix_[i][j] = 0;
								if (i == j) matrix_[i][j] = 1;
							}
						}
					}
					if (x == 1) {
						for (int i = 0; i < n; i++) {
							for (int j = 0; j < m; j++) {
								matrix_[i][j] = matrix0[i][j];
							}
						}
					}
				}
				else {
					for (int i = 0; i < n; i++) {
						for (int j = 0; j < m; j++) {
							matrix_[i][j] = matrix0[i][j];
							matrix9[i][j] = 0;
						}
					}

					for (int k = 1; k < x; k++) {
						for (int i = 0; i < n; i++) {
							for (int j = 0; j < m; j++) {
								for (int p = 0; p < n; p++) {
									matrix9[i][j] += matrix0[i][p] * matrix_[p][j];
								}
							}
						}
						for (int i = 0; i < n; i++) {
							for (int j = 0; j < m; j++) {
								matrix_[i][j] = matrix9[i][j];
								matrix9[i][j] = 0;
							}
						}
					}
				}
				for (int i = 0; i < n; i++) {
					for (int j = 0; j < m; j++) {
						matrix9[i][j] = matrix_[i][j] * (index + 1);
						matrix_[i][j] = matrix9[i][j];
					}
				}
				index = 0;
				TF = true;
			}
					  break;
			case '+': TF = true; break;
			case '-': TF = true; break;
			case 't': break;
			default: {
				int c = 0;
				string strp = str;
				for (int j = i + 1; j < str.length(); j++) {
					if (isdigit(str[j])) {
						c++;
						str[j] = 't';
					}
					else {
						break;
					}
				}

				strp.erase(0, i);
				strp.erase(i + c + 1, str.length() - c - 1);
				index1 = atoi(strp.c_str());
				index = atoi(strp.c_str()) - 1;
			}
					 break;
			}
			if (TF == true) {
				break;
			}
		}

		if (index != 0) {
			for (int i = 0; i < n; i++)
				for (int j = 0; j < m; j++) {
					matrix_[i][j] = 0;
					if (i == j) matrix_[i][j] = index + 1;
				}
		}
		if ((index == 0) && (index1 == 1) && (noexp == 0)) {
			for (int i = 0; i < n; i++)
				for (int j = 0; j < m; j++) {
					matrix_[i][j] = 0;
					if (i == j) matrix_[i][j] = 1; //Единичная матрица
				}
		}
	}

	for (int i = 0; i < n; i++)
		free(matrix9[i]);
	free(matrix9);

	return matrix_;
}

void GetMatrix(int **mas, int **p, int i, int j, float m) {

	int ki, kj, di, dj;
	di = 0;

	for (ki = 0; ki < m - 1; ki++) { // проверка индекса строки
		if (ki == i) {
			di = 1;
		}
		dj = 0;

		for (kj = 0; kj < m - 1; kj++) { // проверка индекса столбца
			if (kj == j) {
				dj = 1;
			}
			p[ki][kj] = mas[ki + di][kj + dj];
		}
	}
}

int Determinant(int **matrix0, int m) {
	int i, j, d, k, n;

	int **p = new int*[m];
	for (i = 0; i < m; i++)
		p[i] = new int[m];

	j = 0;
	d = 0;
	k = 1;
	n = m - 1;

	if (m < 1) cout << "Определитель вычислить невозможно!";

	if (m == 1) {
		d = matrix0[0][0];

		for (int i = 0; i < n; i++) {
			free(p[i]);
		}
		free(p);

		return(d);
	}

	if (m == 2) {
		d = matrix0[0][0] * matrix0[1][1] - (matrix0[1][0] * matrix0[0][1]);

		for (int i = 0; i < n; i++) {
			free(p[i]);
		}
		free(p);

		return(d);
	}

	if (m > 2) {
		for (i = 0; i < m; i++) {
			GetMatrix(matrix0, p, i, 0, m);
			d = d + k * matrix0[i][0] * Determinant(p, n);
			k = -k;
		}

		for (int i = 0; i < n; i++) {
			free(p[i]);
		}
		free(p);

		return(d);
	}

	for (int i = 0; i < n; i++) {
		free(p[i]);
	}
	free(p);

	return(d);
}

void arr_append(int M, size_t size, int elem) {
int *M1 = new int [size + 1];
for (size_t i = 0; i < size; i++)
//M1[i] = M[i];
M1[size] = elem;
//delete[] M;
//M = M1;
size++;
}


int main(int argc, char* argv[]) {

	setlocale(LC_ALL, "Rus");
	int N;
	bool TF;

	int** matrix = 0;
	int** matrix1 = 0;
	int n = 0; /*Размерность матрицы nxm*/
	int m = 0;
	int w = 0;

	if (argc >= 2) {
		n = atoi(argv[1]); //преобразование строки в число
		m = atoi(strchr(argv[1], 'x') + 1); //преобразование строки в число
	}
	if (n*m != 0) {
		w = 1;
		//Создаём динамический массив:
		matrix = new int *[n]; /* **matrix - указатель на указатель
							   new - выделение памяти под n элементов*/
		for (int i = 0; i < n; i++) {
			matrix[i] = new int[m] {0}; //заполняем оставшиеся элементы нулями, если введено меньшее количество
		}
	}

	if (argc == 1) {
		TF = true;
	}
	if (argc == 2) {
		TF = true;
	}
	if (argc >= 3) {

		if (argc == 3) {

			bool TF = false;
			char* open = argv[2];
			for (int i = 0; i < atoi(argv[1]); i++) {
				for (int j = 0; j < atoi(strchr(argv[1], 'x') + 1); j++) {
					matrix[i][j] = atoi(open);
					if (strchr(open, ',') == 0) {
						TF = true;
						break; //Если конец строки - делаем выход
					}
					open = strchr(open, ',') + 1; //Пропускаем ','
				}
				if (TF) {
					break;
				}
			}
		}

		if (argc > 3) {
			int c = 2;
			for (int i = 0; i < atoi(argv[1]); i++) {
				for (int j = 0; j < (atoi(strchr(argv[1], 'x') + 1)) && c < argc; j++, c++) {
					matrix[i][j] = atoi(argv[c]);
				}
			} //Ввод с 3 аргумента элементов матрицы nxm
		}
		TF = false;
	};


	for (;;) {

		std::cout << "Выберите одну из операций:" << endl;
		std::cout << "1. Вывести матрицу" << endl;
		std::cout << "2. Сложить матрицу" << endl;
		std::cout << "3. Умножить матрицу" << endl;
		std::cout << "4. Транспонировать матрицу" << endl;
		std::cout << "5. Расширить матрицу" << endl;
		std::cout << "6. Найти элементы" << endl;
		std::cout << "7. Изменить значение элемента" << endl;
		std::cout << "8. Возвести в степень" << endl;
		std::cout << "9. Вычислить определитель матрицы" << endl;
		std::cout << "10. Вычислить обратную матрицу" << endl;
		std::cout << "11. Вычислить многочлен матрицы" << endl;
		std::cout << "12. Сохранить в файл" << endl;
		std::cout << "13. Загрузить из файла" << endl;
		std::cout << "14. Сортировать матрицу" << endl;
		std::cout << "15. Выйти из программы" << endl;

		std::cout << "Введите номер операции: ";
		std::cin >> N;


		switch (N) {
		case 1: {
			if (TF == true) {
				std::cout << "Матрица не заполнена" << endl;
			}
			else {
				for (int i = 0; i < atoi(argv[1]); ++i) {
					for (int j = 0; j < atoi(strchr(argv[1], 'x') + 1); ++j) {
						std::cout << matrix[i][j] << " ";
					}
					std::cout << endl;
				}
			}
			break;
		};
		case 2: {
			if (TF == true) {
				std::cout << "Матрица не заполнена" << endl;
				break;
			}
			else {
				std::cout << "Введите матрицу: ";

				int **matrix1 = new int*[n];
				for (int i = 0; i < atoi(argv[1]); ++i) {
					matrix1[i] = new int[m] {0};
				}

				int **matrix2 = new int*[n];
				for (int i = 0; i < atoi(argv[1]); i++) {
					matrix2[i] = new int[m] {0};
				}

				int v;//элемент новой матрицы

				std::cout << "Введите новую матрицу " << n << "v" << m << ": ";
				for (int i = 0; i < n; i++) {
					for (int j = 0; j < m; j++) {
						std::cin >> v;
						matrix1[i][j] = v;
					}
				}

				std::cout << "Сумма матриц: " << endl;
				for (int i = 0; i < n; i++) {
					for (int j = 0; j < m; ++j) {
						matrix2[i][j] = matrix[i][j] + matrix1[i][j];
						std::cout << matrix2[i][j] << " ";
					}
					std::cout << endl;
				}

				for (int i = 0; i < n; i++) {
					free(matrix1[i]);
				}
				for (int i = 0; i < n; i++) {
					free(matrix2[i]);
				}
				free(matrix1);
				free(matrix2);
				break;
			}
			break;
		};
		case 3: {

			char v[256];
			int n1 = 0;
			int m1 = 0;

			std::cout << "Введите размер матрицы: ";
			std::cin >> v;
			n1 = atoi(v);
			m1 = atoi(strchr(v, 'x') + 1);

			if (n1*m1 == 0) {
				std::cout << "Матрица пустая." << endl;
				continue;
			}

			if (n1 != m) {
				std::cout << "Ошибка. Введён неверный рзмер матрицы." << endl;
				continue;
			}

			else {
				std::cout << "Введите матрицу: ";

				int **matrix3 = new int*[n1];
				for (int i = 0; i < n1; i++) {
					matrix3[i] = new int[m1];
				}

				for (int i = 0; i < n1; i++) {
					for (int j = 0; j < m1; j++) {
						matrix3[i][j] = 0;
					}
				}

				for (int i = 0; i < n1; i++) {
					for (int j = 0; j < m1; j++) {
						std::cin >> matrix3[i][j];
					}
				}

				std::cout << "Произведение матриц:";

				for (int i = 0; i < n; i++) {
					for (int k = 0; k < m1; k++) {
						int pr_matrix = 0;
						for (int j = 0; j < m; j++) {
							pr_matrix += matrix[i][j] * matrix3[j][k];
						}
						std::cout << pr_matrix << " ";
					}
					std::cout << endl;
				}

				for (int i = 0; i < n1; i++) {
					free(matrix3[i]);
				}
				free(matrix3);
				free(v);
			}
			break;
		};
		case 4: {

			int c = m;
			m = n;
			n = c;

			int **matrix4 = new int*[n];
			for (int i = 0; i < n; i++) {
				matrix4[i] = new int[m];
				for (int j = 0; j < m; j++) {
					matrix4[i][j] = matrix[j][i];
				}
			}

			cout << "Транспонированная матрица: " << endl;
			for (int i = n - 1; i >= 0; i--) {
				for (int j = 0; j < m; j++) {
					cout << matrix4[i][j] << " ";
				}
				cout << endl;
			}

			for (int i = 0; i < m; i++) {
				free(matrix4[i]);
			}
			free(matrix);
			break;
		};
		case 5: {
			char x5;
			cout << "Введите направление: " << endl;
			cout << "h" << endl; //влево
			cout << "k" << endl; //вверх
			cout << "l" << endl; //вправо
			cout << "j" << endl; //вниз
			cin >> x5;
			if (x5 == 'h' || x5 == 'l') {
				int **matrix5 = new int*[n];
				for (int i = 0; i < n; i++)
					matrix5[i] = new int[m + 1];

				if (x5 == 'h') {
					for (int i = 0; i < n; i++)
						for (int j = 1; j < m + 1; j++) {
							matrix5[i][j] = matrix[i][j - 1];
							cout << matrix5[i][j] << " ";
						}
					cout << endl;
					m = m + 1;
				}
				if (x5 == 'l') {
					for (int i = 0; i < n; i++)
						for (int j = 0; j < m + 1; j++) {
							if (j != m) {
								matrix5[i][j] = matrix[i][j];
							}
							else matrix5[i][j] = 0;
							cout << matrix5[i][j] << " ";
						}
					m = m + 1;
					cout << endl;
				}
				for (int i = 0; i < n; i++) {
					free(matrix[i]);
				}
				free(matrix);
			}

			if (x5 == 'k' || x5 == 'j') {
				int **matrix5 = new int*[n + 1];
				for (int i = 0; i < n + 1; i++)
					matrix5[i] = new int[m];

				if (x5 == 'k') {
					for (int i = 1; i < n + 1; i++)
						for (int j = 0; j < m; j++) {
							matrix5[i][j] = matrix[i - 1][j];
							cout << matrix5[i][j] << " ";
						}
					cout << endl;
					n = n + 1;
				}

				if (x5 == 'j') {
					for (int i = 0; i < n; i++)
						for (int j = 0; j < m; j++) {
							matrix5[i][j] = matrix[i][j];
						}
					n = n + 1;
				}

				for (int i = 0; i < n + 1; i++) {
					free(matrix[i]);
				}
				free(matrix);
			}
			else {
				cout << "Некорректное направление" << endl;
				return 0;
			}
			break;
		};
		case 6: {
			int x;
			cout << "Введите значение: ";
			cin >> x;
			cout << endl;

			int f = 0; //TF
			cout << "Результат: ";

			for (int i = 0; i < n; i++) {
				for (int j = 0; j < m; j++) {
					if (matrix[i][j] == x && f != 0) {
						cout << ", ";
						cout << "[" << i << "]" << " [" << j << "]";
					}
					if ((matrix[i][j] == x) && (f == 0)) {
						cout << "[" << i << "]" << " [" << j << "]";
						f++;
					}
				}
			}
			if (f == 0) {
				cout << "Элемент не найден!";
			}
			cout << endl;
			cout << endl;
			break;
		};
		case 7: {
			int i1 = 0, j1 = 0; //[i1][j1] - элемент, который будем менять
			char x;
			cout << "Введите позицию ([i][j]) для изменения значения: ";
			cin >> x >> i1 >> x >> x >> j1 >> x;

			if (i1 > n - 1 || j1 > m - 1) {
				cout << "Некорректная позиция" << endl;
			}
			else {
				int newval = 0; //newvalue(newval) - новое значение элемента
				cout << "Введите новое значение элемента: ";
				cin >> newval;
				matrix[i1][j1] = newval;
			}
			break;
		};
		case 8: {
			float a; //Степень
			cout << "Укажите степень: ";
			cin >> a;
			int b = a;

			if (a == 1) {
				cout << "Результат: " << endl; //Матрица не изменяется
				for (int i = 0; i < n; i++) {
					for (int j = 0; j < m; j++) {
						cout << matrix[i][j] << " ";
					}
					cout << endl;
				}
			}

			if (a < 0) {
				cout << "Степень должна быть неотрицательной!" << endl;
			}

			if (a == 0) {
				cout << "Результат: " << endl; //Получим единичную матрицу
				for (int i = 0; i < n; i++) {
					for (int j = 0; j < m; j++) {
						if (i != j) matrix[i][j] = 0;
						else matrix[i][j] = 1; //Главная диагональ
						cout << matrix[i][j] << " ";
					}
					cout << endl;
				}
			}

			if (a - b != 0) {
				cout << "Степень должна быть целым числом!" << endl;
			}

			if ((a > 1) && (a - b == 0)) {
				if (m == n) {
					int **matrix7 = new int*[n];
					for (int i = 0; i < n; i++) {
						matrix7[i] = new int[m];
					}

					int **matrix_ = new int*[n];
					for (int i = 0; i < n; i++) {
						matrix_[i] = new int[m];
					}

					for (int i = 0; i < n; i++) {
						for (int j = 0; j < m; j++) {
							matrix7[i][j] = matrix_[i][j];
							matrix_[i][j] = 0;
						}
					}

					for (int k = 1; k < a; k++) {
						for (int i = 0; i < n; i++) {
							for (int j = 0; j < m; j++) {
								for (int p = 0; p < n; p++) {
									matrix_[i][j] += matrix_[i][p] * matrix7[p][j];
								}
							}
						}
						for (int i = 0; i < n; i++) {
							for (int j = 0; j < m; j++) {
								matrix7[i][j] = matrix_[i][j];
								matrix_[i][j] = 0;
							}
						}
					}

					cout << "Результат: " << endl;

					for (int i = 0; i < n; i++) {
						for (int j = 0; j < m; j++) {
							cout << matrix7[i][j] << " ";
						}
						cout << endl;
					}

					for (int i = 0; i < n; i++) {
						free(matrix7[i]);
					}
					free(matrix7);

					for (int i = 0; i < n; i++) {
						free(matrix_[i]);
					}
					free(matrix_);
				}
				else {
					cout << endl;
					cout << "Неверный размер" << endl;
					cout << endl;
				}
			}
			break;
		};
		case 9: {
			if (n != m) {
				cout << "Матрица не квадратная!" << endl;
			}
			else {
				int d = 0;
				d = Determinant(matrix, m); //Определитель
				cout << "Результат: " << d << endl;
			}
			break;
		};
		case 10: {
			if (n != m)
				cout << "Матрица не квадратная!" << endl;
			else {
				int d = 0;

				double **inverse = new double*[m];
				for (int i = 0; i < m; i++)
					inverse[i] = new double[m];

				double **inverse1 = new double*[m];
				for (int i = 0; i < m; i++)
					inverse1[i] = new double[m];


				d = Determinant(matrix, m);

				if (d) {
					for (int i = 0; i < n; i++) {
						for (int j = 0; j < n; j++) {
							int mm = n - 1;

							int **temp_matr = new int *[mm];
							for (int k = 0; k < mm; k++)
								temp_matr[k] = new int[mm];

							GetMatrix(matrix, temp_matr, i, j, n);

							inverse[i][j] = pow(double(-1.0), i + j + 2) * Determinant(temp_matr, mm) / d; //pow - возведение в степень

							if (inverse[i][j] = 0) {
								inverse[i][j] = 0;
							}

							for (int i = 0; i < mm; i++) {
								free(temp_matr[i]);
							}
							free(temp_matr);
						}
					}

					cout << "Обратная матрица: " << endl;
					for (int i = 0; i < n; i++) {
						for (int j = 0; j < n; j++) {
							inverse1[j][i] = inverse[i][j];
						}
					}

					for (int i = 0; i < n; i++) {
						for (int j = 0; j < n; j++) {
							inverse1[i][j] = round(inverse1[i][j] * 1000) / 1000;
							cout << inverse1[i][j] << " ";
						}
						cout << endl;
					}
				}
				if (!d) {
					cout << "Не имеет обратной матрицы!" << endl;
				}

				for (int i = 0; i < n; i++) {
					free(inverse[i]);
				}
				free(inverse);
				for (int i = 0; i < n; i++) {
					free(inverse1[i]);
				}
				free(inverse1);
			}
			break;
		};
		case 11: {
			string str;
			cout << "Введите многочлен: ";
			cin >> str;

			int deg = 0; // Максимальная степень
			int deg1 = 0; // -\\-
			for (int i = 0; i < str.length(); i++) {
				string str1 = str;
				if (str[i] == '^') {
					str1.erase(0, i + 1); //Удаление элементов с первого по i+2
					if ((isdigit(str[i + 2])) || (str[i + 2] != '\0')) { //isdigit - проверка на десятичное число
						str1.erase(2, str.length() - 2);
						deg = deg1;
						deg1 = atoi(str1.c_str());
						if (deg1 > deg) deg = deg1;
					}
					else {
						str1.erase(1, str.length() - 1);
						deg = deg1;
						deg1 = atoi(str1.c_str());
						if (deg1 > deg) deg = deg1;
						break;
					}
				}
			}
			if ((n != m) && (deg > 1))
				cout << "Матрица не квадратная!" << endl;
			else {
				int **matrix1 = new int*[n];
				for (int i = 0; i < n; i++)
					matrix1[i] = new int[m];

				string str2 = str;
				int p = 0;

				for (int i = 0; i < str2.length(); i++) {
					p++;
					switch (str2[i]) {
					case '+': {
						str.erase(0, p);
						p = 0;
						matrix1 = polynomial(matrix, n, m, str);
						for (int j = 0; j < n; j++) {
							for (int k = 0; k < m; k++) {
								matrix[j][k] = matrix[j][k] + matrix1[j][k];
							}
						}
					}
							  break;
					case '-': {
						str.erase(0, p);
						p = 0;
						matrix1 = polynomial(matrix, n, m, str);
						for (int j = 0; j < n; j++) {
							for (int k = 0; k < m; k++) {
								matrix[j][k] = matrix[j][k] - matrix1[j][k];
							}
						}
					}
							  break;
					}

				}

				for (int i = 0; i < n; i++) {
					for (int j = 0; j < m; j++) {
						cout << matrix[i][j] << " ";
					}
					cout << endl;
				}

				for (int i = 0; i < n; i++) {
					free(matrix[i]);
				}
				free(matrix);
				for (int i = 0; i < n; i++) {
					free(matrix1[i]);
				}
				free(matrix1);
			}
			break;
		};
		case 12: {
			string filename;

			cout << "Укажите путь к файлу: ";
			cin >> filename;

			ifstream fin(filename.c_str());

			if (!fin.is_open()) {
				ofstream fout(filename.c_str());

				for (int i = 0; i < n; ++i) {
					for (int j = 0; j < m; ++j) {
						fout << matrix[i][j] << " ";
					}
					fout << endl;
				}
				fout.close();
			}
			else {
				string answer;

				cout << "Перезаписать файл? (y/n)" << endl;
				cin >> answer;

				if ((answer == "Y") || (answer == "y") || (answer == "YES") || (answer == "Yes") || (answer == "yes")) {
					ofstream fout(filename.c_str());

					for (int i = 0; i < n; ++i) {
						for (int j = 0; j < m; ++j) {
							fout << matrix[i][j] << " ";
						}
						fout << endl;
					}
					fout.close();
				}
				else {
					return 0;
				}
			}
			break;
		};
		case 13: {
			cout << "Укажите путь к файлу: ";

			string path;
			getline(cin, path);
			if (path.empty()){
				cout << "Ничего не введено!\n";
				return 0;
			}

			ifstream fin;
			fin.open(path, ifstream::in);
			if (!fin.good()){
				cout << "Ошибка при открытии файла!\n";
				return 0;
			}

			for (size_t i = 0; i < n; i++)
				delete[] matrix[i];
			delete[] matrix;

			int m = NULL;
			size_t col = 0;
			int elem;

			while (fin.good()) {
				col = 0;
				m = NULL;

				while (fin.peek() != '\n' && fin.peek() != EOF) {
					fin >> elem;
					arr_append(m, col, elem);
				}
				fin.get();
				arr_append(**matrix, n, m);
			}
			fin.close();
			m = col;
			break;
		};
		case 14: {
			char x5;
			cout << "Выберите порядок сортировки: " << endl;
			cout << "s" << endl;
			cout << "e" << endl;
			cout << "a" << endl;

			cin >> x5;

			for (int k = 0; k < n*m; k++)
				for (int i = 0; i < n; i++)
					for (int j = 0; j < m - 1; j++) {
						if (matrix[i][j] > matrix[i][j + 1]) {
							int count = matrix[i][j];
							matrix[i][j] = matrix[i][j + 1];
							matrix[i][j + 1] = count;
						}
						if (i < n - 1)
							if ((matrix[i][j + 1] > matrix[i + 1][0]) && (j == m - 2)) {
								int count = matrix[i][j + 1];
								matrix[i][j + 1] = matrix[i + 1][0];
								matrix[i + 1][0] = count;
							}
					}
			switch (x5)
			{
			case 'a': {
				break;
			}
					  break;

			case 's': {
				int **b = new int*[n];
				for (int i = 0; i < n; i++)
					b[i] = new int[m];

				int k = 0, l = 0;
				for (int i = 0; i < n; i++)
					for (int j = 0; j < m; j++)
					{
						if ((k < n) && (l % 2 == 0))
						{
							b[k][l] = matrix[i][j];
							k++;
						}
						else if (k == n)
						{
							k--;
							l++;
						}
						if ((k >= 0) && (l % 2 == 1))
						{
							b[k][l] = matrix[i][j];
							k--;
						}
						else if (k < 0)
						{
							k++;
							l++;
							b[k][l] = matrix[i][j];
							k++;
						}
					}

				for (int i = 0; i < n; i++)
					for (int j = 0; j < m; j++)
						matrix[i][j] = b[i][j];

				for (int i = 0; i < n; i++)
					delete[] b[i];
				delete[] b;
			}
					  break;

			case 'e': {
				int **a = new int*[n];
				for (int i = 0; i < n; i++)
					a[i] = new int[m];

				for (int q = 0; q < n; q++)
					for (int w = 0; w < m; w++) {
						a[q][w] = matrix[q][w];
					}

				int i = 0, j, k = 0, p = 0, t = 0;

				while (i < n * m) {
					k++;
					for (j = k - 1; j < m - k + 1; j++) {
						matrix[k - 1][j] = a[p][t];
						if (t + 1 < m)	t++;
						else {
							t = 0;
							p++;
						}
						i++;
					}

					for (j = k; j < n - k + 1; j++) {
						matrix[j][m - k] = a[p][t];

						if (t + 1 < m)	t++;
						else {
							t = 0;
							p++;
						}
						i++;
					}

					for (j = m - k - 1; j >= k - 1; j--) {
						matrix[n - k][j] = a[p][t];

						if (t + 1 < m)	t++;
						else {
							t = 0;
							p++;
						}
						i++;
					}
					for (j = n - k - 1; j >= k; j--) {
						matrix[j][k - 1] = a[p][t];

						if (t + 1 < m)	t++;
						else {
							t = 0;
							p++;
						}
						i++;
					}
				}
				for (int i = 0; i < n; i++)
					delete[] a[i];
				delete[] a;
			}
			}
			break;
		};
		case 15: {
			string str;

			std::cout << "Вы хотите выйти из программы?  (y/N):";

			std::cin >> str;

			if ((str == "y") || (str == "yes") || (str == "Yes") || (str == "YES") || (str == "Y")) {
				if (matrix) {
					for (int i = 0; i < n; i++)
						free(matrix[i]);
				}
				free(matrix);
				std::cout << "До свидания!" << endl;
				exit(1);
			};
			if ((str == "n") || (str == "no") || (str == "No") || (str == "NO") || (str == "N")) {
				std::cout << " Продолжение работы" << endl;
				continue;
			}
			else std::cout << "Ошибка ввода" << endl;
		};
		};
	}

	for (int i = 0; i < n; i++) {
		free(matrix[i]); //Удаляем выделенную память
	}
	free(matrix);

	system("pause");
	return 0;

}